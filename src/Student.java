import java.util.Objects;

public class Student implements Comparable<Student>{

    private String strFirstNameMapped;
    private String strLastNameMapped;
    private int nStudentIdMapped;

    //constructor to create student object
    public Student(String strFirstName, String strLastName, int nStudentId ){
        this.strFirstNameMapped = strFirstName;
        this.strLastNameMapped = strLastName;
        this.nStudentIdMapped = nStudentId;

    }

    public String getFirstName(){
        return strFirstNameMapped;
    }

    public String getLastName(){
        return strLastNameMapped;
    }

    public int getStudentId(){
        return nStudentIdMapped;
    }

    /*
    compare last names of students
    then first names if last names are equal
    then student IDs if first and last names are equal
     */
    @Override
    public int compareTo(Student studentObject) {
        Student compareObject = (Student) studentObject;

        int studentNameCompare = this.strLastNameMapped.compareTo(compareObject.getLastName());
        if(studentNameCompare > 0){
            return 1;
        } else if(studentNameCompare < 0){
            return -1;
        }
        if(studentNameCompare == 0){
            studentNameCompare = this.strFirstNameMapped.compareTo(compareObject.getFirstName());
            if(studentNameCompare > 0){
                return 1;
            } else if(studentNameCompare < 0){
                return -1;
            }
            if(studentNameCompare == 0){
                if(nStudentIdMapped == studentObject.nStudentIdMapped){
                    return 0;
                }else if(nStudentIdMapped > studentObject.nStudentIdMapped){
                    return 1;
                }else {
                    return -1;
                }
            }
        }
        return 1;
    }

    @Override
    public String toString(){
        return strFirstNameMapped + " " + strLastNameMapped + " (ID=" + nStudentIdMapped + ") ";
    }

    //@Override
    //public boolean equals(Student hashOfStudentId){
    //    Student compareId = (Student) hashOfStudentId;
    //    return true;
   // }

    @Override
    public int hashCode(){
        return Objects.hash(nStudentIdMapped);
    }


}
