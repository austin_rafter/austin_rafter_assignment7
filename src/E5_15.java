import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class E5_15 {
    public static void main(String[] args) {
        File readJavaFile;
        Scanner scanFileForIdentifiers = new Scanner(System.in);
        Scanner scanLinesOfFile = new Scanner(System.in);

        //String strFileName = "src/Main.java";


        //command line argument is file name
        String strFileName = args[0];

        //set file if file exists
        //pass file variable into scanners for identifiers and lines
        try {
            readJavaFile = new File(strFileName);
            scanFileForIdentifiers = new Scanner(readJavaFile);
            scanLinesOfFile = new Scanner(readJavaFile);
        } catch(FileNotFoundException fileNotFound) {
            System.out.println("File Not Found");
        }


        //Create a map to store the identifiers and indexes
        LinkedHashMap<String,Integer> identifiersAndIndexes = new LinkedHashMap<>();

        //set initial index
        int nIndexOfIdentifier = 0;


        //set delimiter to only get identifiers
        scanFileForIdentifiers.useDelimiter("[^A-Za-z0-9_]+");

        //go through file storing each identifier into a treemap with its index
        //no identifiers will be repeated in the map
        while(scanFileForIdentifiers.hasNext()){

            //set next identifier to a string
            String strIdentifier = scanFileForIdentifiers.next();


            //check if the identifier is already mapped
            //if it is set it some jumbled text that would not likely
            //be in a java file
            Set<String> keySet = identifiersAndIndexes.keySet();
            for(String identifierTest : keySet){
                if(strIdentifier.equals(identifierTest)){
                 strIdentifier = "gKJLSDEGLJGWDLJQWHDLKhsdfliuASDHLiuadhyKJHSDLKHlskjdhlKJSDHKJHASDKUQHDWIUDHAUSHDLkusgdlGAYDIUaghsldiuh";
                 nIndexOfIdentifier--; //subtract from index to maintain index of first appearance of identifiers
                }
            }


            //if the identifier is not the jumbled text
            //print it with its index
            if(strIdentifier != "gKJLSDEGLJGWDLJQWHDLKhsdfliuASDHLiuadhyKJHSDLKHlskjdhlKJSDHKJHASDKUQHDWIUDHAUSHDLkusgdlGAYDIUaghsldiuh") {
                System.out.println(nIndexOfIdentifier + ":  " + strIdentifier + " occurs in ");
            }
            //go through file checking if identifier is in any lines
            //if it is print the strings that contain it
            while(scanLinesOfFile.hasNext()){
                String strLinesFromFile = scanLinesOfFile.nextLine();

                if(strLinesFromFile.contains(strIdentifier)) {
                    //split string to ensure identifier is present alone
                    //and not just part of a word in the line
                    String[] splitterOfFileStrings = strLinesFromFile.split("[^a-zA-Z0-9_']+");
                    for(int nSplitIdentifier = 0; nSplitIdentifier < splitterOfFileStrings.length; nSplitIdentifier++){
                        //if identifier is present in split string then print the string
                        if(splitterOfFileStrings[nSplitIdentifier].equals(strIdentifier)){
                            System.out.println(strLinesFromFile);
                        }

                    }

                }
            }

            //add identifier and index of identifier to a map
            identifiersAndIndexes.putIfAbsent(strIdentifier, nIndexOfIdentifier++);

            //open new file to search through lines with next
            //identifier
            try {
                File readJavaFileAgain = new File(strFileName);
                scanLinesOfFile = new Scanner(readJavaFileAgain);
            } catch(FileNotFoundException fileNotFound) {
                System.out.println("File Not Found");
            }

        }

    }
}
