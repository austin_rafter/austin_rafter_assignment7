import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class E15_4 {
    public static void main(String[] args) {

        //create tree map for students and names
        Map<String, String> mapOfStudents = new TreeMap<String, String>();

        Scanner enterChoice = new Scanner(System.in);

        //ask user to enter option
        System.out.println("Select an option:\n" +
                "(a) to add a student\n" +
                "(r) to remove a student\n" +
                "(m) to modify a grade\n" +
                "(p) print all grades\n" +
                "(q) to quit");
        //user choice entry
        String strChoiceEntered = enterChoice.nextLine();
        //create some students for testing
        mapOfStudents.put("Rick James", "B-");
        mapOfStudents.put("Neil deGrasse Tyson", "A");
        mapOfStudents.put("George Washington", "B");

        //when user enters q quit out
        while(strChoiceEntered.toUpperCase().charAt(0) != 'Q'){
            //when user enters a have them enter name and grade
            if(strChoiceEntered.toUpperCase().charAt(0) == 'A'){
                System.out.println("Enter student name to add: ");
               String strName = enterChoice.nextLine();
                System.out.println("Enter student grade to add: ");
                String strGrade = enterChoice.nextLine();

                //add entered name and grade to the map
                mapOfStudents.put(strName, strGrade);

            } else if(strChoiceEntered.toUpperCase().charAt(0) == 'R'){
                //if user entered r have them enter the name of the student
                System.out.println("Enter name of student to remove: ");
                String strRemoveNameAndGrade = enterChoice.nextLine();
                //remove the student
                mapOfStudents.remove(strRemoveNameAndGrade);

            } else if(strChoiceEntered.toUpperCase().charAt(0) == 'M'){
                //if user entered m have them enter student name and new grade
                System.out.println("Enter name of student whose grade you want to change: ");
                String strName = enterChoice.nextLine();
                System.out.println("Enter student new grade: ");
                String strNewGrade = enterChoice.nextLine();
                //update the student in the map
                mapOfStudents.put(strName, strNewGrade);

            } else if(strChoiceEntered.toUpperCase().charAt(0) == 'P'){
                //create iterator to iterate through the map
                Iterator iterateGrades = mapOfStudents.entrySet().iterator();

                //check that the map has another student
                //if map does print key and value
                while( iterateGrades.hasNext()){
                    //https://stackoverflow.com/questions/13642636/treemap-how-does-it-sort
                    Map.Entry gradesAndNames = (Map.Entry)iterateGrades.next();
                    System.out.println(gradesAndNames.getKey() + ":" + gradesAndNames.getValue());
                }

            } else{
                System.out.println("Please enter a choice");
            }

            System.out.println("Select an option:\n" +
                    "(a) to add a student\n" +
                    "(r) to remove a student\n" +
                    "(m) to modify a grade\n" +
                    "(p) print all grades\n" +
                    "(q) to quit");
             strChoiceEntered = enterChoice.nextLine();

        }

    }

}
