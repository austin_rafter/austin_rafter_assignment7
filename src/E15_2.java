import java.util.*;

public class E15_2 {
    public static void main(String[] args) {

        //create map to store student objects and grades
        Map<Student,String> mapOfStudents = new TreeMap<Student, String>();


        Scanner enterChoice = new Scanner(System.in);

        //have user enter first choice
        System.out.println("Select an option:\n" +
                "(a) to add a student\n" +
                "(r) to remove a student\n" +
                "(m) to modify a grade\n" +
                "(p) print all grades\n" +
                "(q) to quit");
        String strChoiceEntered = enterChoice.nextLine();
        //until user enters Q for quit choice
        while(strChoiceEntered.toUpperCase().charAt(0) != 'Q'){

            //if user enters A for add choice
            if(strChoiceEntered.toUpperCase().charAt(0) == 'A'){

                Scanner addStudent = new Scanner(System.in);

                //user enters student first name
                System.out.println("Enter student first name to add: ");
                String strFirstName = addStudent.nextLine();

                //user enters student last name
                System.out.println("Enter student last name to add: ");
                String strLastName = addStudent.nextLine();

                //user enters student grade
                System.out.println("Enter student grade to add: ");
                String strGrade = addStudent.nextLine();

                //user enters student ID
                System.out.println("Enter student ID: ");
                int nStudentId = addStudent.nextInt();



                //set all previously mapped students into a set
                Set<Student> keySet = mapOfStudents.keySet();
                //check the set for the entered ID
                for(Student studentTest : keySet){
                    //if student ID is found tell user multiple students
                    //can't have same ID and have them re-enter
                    if(nStudentId == studentTest.getStudentId()){
                        System.out.println("That ID is already in use. ");
                        System.out.println("Please enter a student ID: ");
                        nStudentId = addStudent.nextInt();
                    }
                }


                mapOfStudents.put(new Student(strFirstName,strLastName, nStudentId), strGrade);

            } else if(strChoiceEntered.toUpperCase().charAt(0) == 'R'){
                /*
                If student enters R choice have them enter student ID to find the student to remove
                and remove the student from the map
                 */
                Scanner enterStudent = new Scanner(System.in);
                System.out.println("Enter ID number of student who you want to remove: ");
                int nStudentIdEntered = enterStudent.nextInt();


                //set map of students to set and find students
                //if ID of a student matches the ID entered then remove the student
                Set<Student> keySet = mapOfStudents.keySet();
                for(Student studentTest : keySet){
                    if(nStudentIdEntered == studentTest.getStudentId()){
                        mapOfStudents.remove(  new Student(studentTest.getFirstName(), studentTest.getLastName(), studentTest.getStudentId()));
                    }
                }

            } else if(strChoiceEntered.toUpperCase().charAt(0) == 'M'){
                //User enters new grade for student
                Scanner modifyStudent = new Scanner(System.in);
                Scanner modifyGrade = new Scanner(System.in);
                System.out.println("Enter ID number of student whose grade you want to change: ");
                int nStudentIdEntered = modifyStudent.nextInt();
                System.out.println("Enter student new grade: ");
                //user enters ID of student whos grade is being changes
                String strGrade = modifyGrade.nextLine();


                //place map of students into a set
                Set<Student> keySet = mapOfStudents.keySet();
                //search through student IDs, when entered ID is found
                //update the grade value within the map
                for(Student studentTest : keySet){
                    if(nStudentIdEntered == studentTest.getStudentId()){
                        mapOfStudents.put(new Student(studentTest.getFirstName(), studentTest.getLastName(), studentTest.getStudentId()), strGrade);
                    }
                }


            } else if(strChoiceEntered.toUpperCase().charAt(0) == 'P'){
                //create iterator to iterate through grades
                Iterator iterateGrades = mapOfStudents.entrySet().iterator();

                //iterate through students in iterator printing the toString for the
                //student object key and the value that is the grade for each student
                while( iterateGrades.hasNext()){
                    //https://stackoverflow.com/questions/13642636/treemap-how-does-it-sort
                    Map.Entry gradesAndNames = (Map.Entry)iterateGrades.next();
                    System.out.println(gradesAndNames.getKey() + ": " + gradesAndNames.getValue());
                }

            } else{
                //ask user to enter a given choice if they entered something not being offered
                System.out.println("Please enter a choice");
            }

            //have user re-enter a choice until they enter the quit choice
            System.out.println("Select an option:\n" +
                    "(a) to add a student\n" +
                    "(r) to remove a student\n" +
                    "(m) to modify a grade\n" +
                    "(p) print all grades\n" +
                    "(q) to quit");
            strChoiceEntered = enterChoice.nextLine();

        }



    }
}
